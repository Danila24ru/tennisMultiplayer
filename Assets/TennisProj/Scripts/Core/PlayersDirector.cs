﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FishNet.Managing;
using FishNet.Object;
using TennisProj.Scripts.Gameplay;
using UnityEngine;

namespace TennisProj.Scripts.Core
{
    public class PlayerSession
    {
        public ulong clientId;
    }

    public class TenisPlayerSession : PlayerSession
    {
        public PlayerScore playerScore;

        public TenisPlayerSession(PlayerScore playerScore)
        {
            this.playerScore = playerScore;
        }
    }
    
    public class PlayersDirector
    {
        private readonly NetworkManager networkManager;
        private readonly PrefabsFactory prefabsFactory;
        private readonly Dictionary<int, NetworkObject> spawnedPlayers;

        public PlayersDirector(NetworkManager networkManager, PrefabsFactory prefabsFactory)
        {
            this.networkManager = networkManager;
            this.prefabsFactory = prefabsFactory;
            
            spawnedPlayers = new Dictionary<int, NetworkObject>();
        }
        
        public IEnumerator SpawnPlayers(Transform[] playerSpawnPositions)
        {
            if (spawnedPlayers.Count == 2 || networkManager.ServerManager.Clients.Count < 2)
                yield break;

            yield return new WaitUntil(() => networkManager.ServerManager.Clients.Values.All(x => x.LoadedStartScenes));
            
            foreach (var connection in networkManager.ServerManager.Clients.Values)
            {
                var spawnedPlayer = prefabsFactory.CreatePlayer(connection, playerSpawnPositions[connection.ClientId].position);
                spawnedPlayers.Add(connection.ClientId, spawnedPlayer);
            }
        }

        public NetworkObject GetPlayerById(int clientId)
        {
            if (spawnedPlayers != null && spawnedPlayers.ContainsKey(clientId))
                return spawnedPlayers[clientId];

            return null;
        }

        public NetworkObject[] GetAllPlayers() => spawnedPlayers?.Values.ToArray();

        public void ClearAllPlayers()
        {
            spawnedPlayers.Clear();
        }
    }
}