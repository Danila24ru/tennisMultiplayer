using System.Collections;
using FishNet.Connection;
using FishNet.Managing;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using TennisProj.ScriptableObjects;
using TennisProj.Scripts;
using TennisProj.Scripts.Gameplay;
using UniRx;
using UnityEngine;
using VContainer;

namespace TennisProj.Scripts.Core
{
    public enum GameState
    {
        None,
        WaitingForPlayers,
        Prepare,
        Game,
        End
    }

    public enum PlayerGameResult
    {
        Win,
        Lose,
        Draw
    }

    public class GoalSignal
    {
        public bool isTop;
    }

    public class ScoreUpdatedSignal
    {
        public int firstPlayerScore;
        public int secondPlayerScore;
    }

    public class GameEndSignal
    {
        public int winnerId;
    }

    public class GameManager : NetworkBehaviour
    {
        public ReactiveProperty<GameState> gameState = new ();
        public ReactiveProperty<int> timeLeft = new ();

        public PlayerGameResult GetLocalPlayerGameResult(int winnerId) =>
            winnerId == int.MaxValue ? PlayerGameResult.Draw :
            winnerId == networkManager.ClientManager.Connection.ClientId ? PlayerGameResult.Win : PlayerGameResult.Lose;

        [Header("Player Setup")]
        [SerializeField] private Transform[] playerSpawnPositions;
    
        private NetworkManager networkManager;
        private GameSettings gameSettings;
        private PrefabsFactory prefabsFactory;
        private BallsDirector ballsDirector;
        private PlayersDirector playersDirector;

        private Coroutine gameTimerCoroutine;

        [Inject]
        public void Init(NetworkManager networkManager, GameSettings gameSettings, PrefabsFactory prefabsFactory, BallsDirector ballsDirector, PlayersDirector playersDirector)
        {
            this.networkManager = networkManager;
            this.gameSettings = gameSettings;
            this.prefabsFactory = prefabsFactory;
            this.ballsDirector = ballsDirector;
            this.playersDirector = playersDirector;

            networkManager.ServerManager.OnRemoteConnectionState += OnRemoteConnectionState;
            networkManager.ServerManager.OnServerConnectionState += OnServerStateChanged;
        }

        private void OnRemoteConnectionState(NetworkConnection connection, RemoteConnectionStateArgs args)
        {
            if (args.ConnectionState == RemoteConnectionState.Started)
            {
                OnClientConnected(connection.ClientId);
            }
        
            if (args.ConnectionState == RemoteConnectionState.Stopped)
            {
                Debug.Log($"Client disconnected: {connection.ClientId}");
            }
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
        
            StartCoroutine(StartWaitForPlayers());
        }

        private void OnServerStateChanged(ServerConnectionStateArgs args)
        {
            switch (args.ConnectionState)  
            {
                case LocalConnectionState.Stopped:
                    playersDirector.ClearAllPlayers();
                    Debug.Log($"Server stopped.");
                    break;
                case LocalConnectionState.Starting:
                    Debug.Log($"Server starting.");
                    break;
                case LocalConnectionState.Started:
                    Debug.Log($"Server started. TotalPlayers: {networkManager.ServerManager.Clients.Count}");
                    break;
                case LocalConnectionState.Stopping:
                    Debug.Log($"Server stopping.");
                    break;
            }
        }

        public void OnClientConnected(int clientId)
        {
            if (!IsServer)
                return;

            var playersCount = networkManager.ServerManager.Clients.Count;
        
            if (playersCount == 2)
            {
                StartCoroutine(PrepareGame());
            }
            else if (playersCount == 1)
            {
                StartCoroutine(StartWaitForPlayers());
            }
        
            Debug.Log($"Client connected: {clientId} TotalPlayers: {playersCount}");
        }
    
        [ObserversRpc(RunLocally = true, BufferLast = true, IncludeOwner = true)]
        private void SyncGameState(GameState gameState)
        {
            this.gameState.Value = gameState;
        }
    
        [ObserversRpc(RunLocally = true)]
        private void SyncTimeLeft(int timeLeft)
        {
            this.timeLeft.Value = timeLeft;
        }

        public IEnumerator StartWaitForPlayers()
        {
            yield return new WaitForSeconds(1);
        
            SyncGameState(GameState.WaitingForPlayers);
            timeLeft.Value = gameSettings.totalGameTimeSeconds;
        }

        public void StartGame()
        {
            MessageBroker.Default.Receive<GoalSignal>().Subscribe(OnGoal).AddTo(this);
            SyncGameState(GameState.Game);
        }

        public IEnumerator PrepareGame()
        {
            SyncGameState(GameState.Prepare);

            yield return playersDirector.SpawnPlayers(playerSpawnPositions);
        
            if(gameTimerCoroutine == null)
                gameTimerCoroutine = StartCoroutine(TickGameTimer());

            yield return RespawnBall();
        
            StartGame();
        }

        private IEnumerator RespawnBall()
        {
            var ball = ballsDirector.AddBall();
            yield return new WaitForSeconds(gameSettings.timeSecondsBeforeStartRound);
            ball.isActive = true;
        }

        public IEnumerator TickGameTimer()
        {
            while (timeLeft.Value > 0)
            {
                timeLeft.Value--;
                SyncTimeLeft(timeLeft.Value);

                yield return new WaitForSeconds(1);
            }

            SelectWinnerAndEndGame();
        }

        private void SelectWinnerAndEndGame()
        {
            int winnerClientId = 0;
            int maxScore = 0;

            var allPlayers = playersDirector.GetAllPlayers();
            for (int i = 0; i < allPlayers.Length; i++)
            {
                var score = allPlayers[i].GetComponent<PlayerScore>().Score;
                if (score > maxScore)
                {
                    maxScore = score;
                    winnerClientId = i;
                }
            }

            if (maxScore == 0)
                winnerClientId = int.MaxValue;
        
            EndGame(winnerClientId);
        }

        private void OnGoal(GoalSignal goalSignal)
        {
            if (!IsServer)
                return;

            var winnerPlayerId = goalSignal.isTop ? 1 : 0;

            var winnerPlayerScore = playersDirector.GetPlayerById(winnerPlayerId).GetComponent<PlayerScore>();
            winnerPlayerScore.IncrementScore();
        
            var firstPlayerScore = playersDirector.GetPlayerById(0).GetComponent<PlayerScore>();
            var secondPlayerScore = playersDirector.GetPlayerById(1).GetComponent<PlayerScore>();

            OnScoreUpdatedClientRpc(firstPlayerScore.Score, secondPlayerScore.Score);
        
            if (winnerPlayerScore.Score >= gameSettings.maxScoreToWin)
            {
                EndGame(winnerPlayerId);
            }
            else
            {
                StartCoroutine(RespawnBall());
            }
        }

        [ObserversRpc(RunLocally = true)]
        private void OnScoreUpdatedClientRpc(int firstPlayerScore, int secondPlayerScore)
        {   
            MessageBroker.Default.Publish(
                new ScoreUpdatedSignal()
                {
                    firstPlayerScore = firstPlayerScore,
                    secondPlayerScore = secondPlayerScore
                });
        }
    
        [ObserversRpc(RunLocally = true)]
        private void OnGameEndClientRpc(int winnerId)
        {   
            MessageBroker.Default.Publish(
                new GameEndSignal()
                {
                    winnerId = winnerId
                });
        }

        public void EndGame(int winnerClientId)
        {
            if(gameTimerCoroutine != null)
                StopCoroutine(gameTimerCoroutine);
        
            ballsDirector.ClearAllBalls();
        
            SyncGameState(GameState.End);
            OnGameEndClientRpc(winnerClientId);
        }
    
        public  void OnDestroy()
        {
            networkManager.ServerManager.OnRemoteConnectionState -= OnRemoteConnectionState;
            networkManager.ServerManager.OnServerConnectionState -= OnServerStateChanged;
        }
    
        public void GoToMenu()
        {
            networkManager.ServerManager.StopConnection(true);
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
    }
}