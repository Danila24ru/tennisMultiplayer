using FishNet;
using FishNet.Managing;
using FishNet.Object;
using FishNet.Transporting;
using TennisProj.ScriptableObjects;
using TennisProj.Scripts.Gameplay;
using TennisProj.Scripts.Gameplay.Abilities;
using TennisProj.Scripts.UI;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace TennisProj.Scripts.Core
{
    public class Bootstrap : LifetimeScope
    {
        [Header("Core")]
        [SerializeField] private GameSettingsSO gameSettingsSo;
        [SerializeField] private NetworkManager networkManager;

        [Header("UI")]
        [SerializeField] private TennisGameViewUI gameViewUI;

        [Header("Prefabs")]
        [SerializeField] private NetworkObject playerPrefab;
        [SerializeField] private Ball ballPrefab;
        [SerializeField] private BaseAbility[] abilitiesPrefabs;
    
        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterComponent(gameObject.AddComponent<CoroutineRunner>());
        
            builder.RegisterInstance(gameSettingsSo.gameSettings);
        
            builder.RegisterComponent(networkManager);
            builder.RegisterComponent(gameViewUI);

            builder.Register<PrefabsFactory>(Lifetime.Singleton)
                .WithParameter("playerPrefab", playerPrefab)
                .WithParameter("ballPrefab", ballPrefab)
                .WithParameter("abilities", abilitiesPrefabs);

            builder.Register<PlayersDirector>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
            builder.Register<BallsDirector>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
            builder.Register<AbilityDirector>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
        
            builder.RegisterComponentInHierarchy<GameManager>();
        
            StartGame();
        }

        void StartGame()
        {
            if (GameStartMode.StartMode == StartMode.None)
                return;

            var transport = InstanceFinder.TransportManager.Transport;
            
            if (GameStartMode.StartMode == StartMode.Host)
            {
                transport.SetServerBindAddress(GameStartMode.ipAddress, IPAddressType.IPv4);
                transport.SetClientAddress(GameStartMode.ipAddress);
                transport.SetPort(7777);
                networkManager.ServerManager.StartConnection();
                networkManager.ClientManager.StartConnection();
            }
            else
            {
                transport.SetClientAddress(GameStartMode.ipAddress);
                transport.SetPort(7777);
                networkManager.ClientManager.StartConnection();
            }
        }
    }
}
