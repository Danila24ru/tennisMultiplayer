using System.Linq;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Utility.Extension;
using TennisProj.Scripts.Gameplay;
using TennisProj.Scripts.Gameplay.Abilities;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace TennisProj.Scripts.Core
{
    public class PrefabsFactory
    {
        private readonly Ball ballPrefab;
        private readonly NetworkObject playerPrefab;
        private readonly BaseAbility[] abilities;
        private readonly IObjectResolver container;

        public PrefabsFactory(IObjectResolver container, Ball ballPrefab, NetworkObject playerPrefab, BaseAbility[] abilities)
        {
            this.container = container;
            this.ballPrefab = ballPrefab;
            this.playerPrefab = playerPrefab;
            this.abilities = abilities;
        }

        public NetworkObject CreatePlayer(NetworkConnection connection, Vector3 spawnPosition)
        {
            Debug.Log($"Spawn player {connection.ClientId}");
        
            var newPlayer = Object.Instantiate(playerPrefab);
        
            container.InjectGameObject(newPlayer.gameObject);
        
            newPlayer.gameObject.SetActive(true);
        
            newPlayer.Spawn(connection);
        
            newPlayer.transform.position = spawnPosition;

            return newPlayer;
        }
    
        public Ball CreateBall(Vector3 spawnBallPosition)
        {
            var ball = Object.Instantiate(ballPrefab, spawnBallPosition, Quaternion.identity);
            container.InjectGameObject(ball.gameObject);
            ball.GetComponent<NetworkObject>().Spawn();
            return ball;
        }

        public BaseAbility CreateRandomAbility(Vector3 spawnPosition)
        {
            if (abilities == null || abilities.Length == 0)
                return null;
        
            var selectedAbilityPrefab = abilities[UnityEngine.Random.Range(0, abilities.Length)];
        
            return CreateAbility(selectedAbilityPrefab, spawnPosition);
        }
    
        public BaseAbility CreateAbility<T>(Vector3 spawnPosition) where T : BaseAbility
        {
            var abilityPrefab = abilities.FirstOrDefault(x => x is T);

            return CreateAbility(abilityPrefab, spawnPosition);
        }

        private BaseAbility CreateAbility(BaseAbility abilityPrefab, Vector3 spawnPosition)
        {
            if (!abilityPrefab)
            {
                Debug.LogError($"Ability is null!");
                return null;
            }
        
            var newAbility = Object.Instantiate(abilityPrefab);
        
            container.InjectGameObject(newAbility.gameObject);
        
            newAbility.GetComponent<NetworkObject>().Spawn();
            newAbility.transform.position = spawnPosition;

            return newAbility;
        }
    }
}
