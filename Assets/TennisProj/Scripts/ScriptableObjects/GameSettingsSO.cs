using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TennisProj.ScriptableObjects
{
    [Serializable]
    public class GameSettings
    {
        public PlayerSettings playerSettings;
        public BallSettings ballSettings;
        public int totalGameTimeSeconds;
        public int maxScoreToWin;
        public int timeSecondsBeforeStartRound;
    }

    [Serializable]
    public class BallSettings
    {
        public float initialSpeed;
        public float maxSpeed;
        public float acceleration;
    }

    [Serializable]
    public class PlayerSettings
    {
        public float acceleration;
        public float maxSpeed;
    }

    [CreateAssetMenu(fileName = "GameSettings", menuName = "GameSettings", order = 0)]
    public class GameSettingsSO : ScriptableObject
    {
        public GameSettings gameSettings;
    }
}