using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace TennisProj.Scripts.UI
{
    public enum StartMode
    {
        None,
        Host,
        Connect
    }

    public static class GameStartMode
    {
        public static StartMode StartMode;
        public static string ipAddress;
    }

    public class MainMenuUI : MonoBehaviour
    {
        [SerializeField] private TMP_InputField ipAddressInput;
    
        [SerializeField] private Button hostButton;
        [SerializeField] private Button connectButton;

        private void Start()
        {   
            hostButton.onClick.AddListener(() =>
            {
                GameStartMode.ipAddress = ipAddressInput.text;
                GameStartMode.StartMode = StartMode.Host;
                SceneManager.LoadScene("Tennis");
            });
        
            connectButton.onClick.AddListener(() =>
            {
                GameStartMode.ipAddress = ipAddressInput.text;
                GameStartMode.StartMode = StartMode.Connect;
                SceneManager.LoadScene("Tennis");
            });
        }
    }
}