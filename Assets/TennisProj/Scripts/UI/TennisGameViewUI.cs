using System;
using System.Collections;
using TennisProj.Scripts.Core;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

namespace TennisProj.Scripts.UI
{
    public class TennisGameViewUI : MonoBehaviour
    {
        [Inject] private GameManager gameManager;

        [SerializeField] private TextMeshProUGUI stateText;

        [SerializeField] private GameObject scorePanel;
        [SerializeField] private TextMeshProUGUI firstPlayerScore;
        [SerializeField] private TextMeshProUGUI secondPlayerScore;

        [SerializeField] private Button exitGame;
    
        private void Start()
        {
            gameManager.gameState.Subscribe(OnGameStateChanged).AddTo(this);
            gameManager.timeLeft.Subscribe(OnTimeLeftChanged).AddTo(this);
        
            exitGame.OnClickAsObservable().Subscribe(x => gameManager.GoToMenu()).AddTo(this);
        
            MessageBroker.Default.Receive<ScoreUpdatedSignal>().Subscribe(OnUpdateScore).AddTo(this);
            MessageBroker.Default.Receive<GameEndSignal>().Subscribe(OnGameEnd).AddTo(this);
        }

        private void OnGameStateChanged(GameState newState)
        {
            stateText.gameObject.SetActive(true);

            switch (newState)
            {
                case GameState.Game:
                    scorePanel.SetActive(true);
                    exitGame.gameObject.SetActive(false);
                    break;
                case GameState.WaitingForPlayers:
                    stateText.text = "Waiting for players...";
                    exitGame.gameObject.SetActive(true);
                    break;
                case GameState.Prepare:
                    stateText.text = "Prepare for game!";
                    exitGame.gameObject.SetActive(false);
                    break;
                case GameState.End:
                    StartCoroutine(OpenExitButton(3));
                    break;
            }
        }

        private IEnumerator OpenExitButton(float delaySeconds)
        {
            yield return new WaitForSeconds(delaySeconds);
            exitGame.gameObject.SetActive(true);
        }
    
        private void OnTimeLeftChanged(int timeLeft)
        {
            if (gameManager.gameState.Value != GameState.Game)
                return;

            var timeLeftSpan = TimeSpan.FromSeconds(timeLeft);
                
            stateText.text = $"Ends in: {timeLeftSpan.Minutes}:{timeLeftSpan.Seconds}";
        }

        private void OnUpdateScore(ScoreUpdatedSignal signal)
        {
            scorePanel.SetActive(true);
            this.firstPlayerScore.text = signal.firstPlayerScore.ToString();
            this.secondPlayerScore.text = signal.secondPlayerScore.ToString();
        }
    
        private void OnGameEnd(GameEndSignal signal)
        {
            stateText.text = gameManager.GetLocalPlayerGameResult(signal.winnerId) == PlayerGameResult.Draw ? "DRAW!" : 
                gameManager.GetLocalPlayerGameResult(signal.winnerId) == PlayerGameResult.Win ? "YOU WIN!" : "YOU LOSE!";
        }
    }
}
