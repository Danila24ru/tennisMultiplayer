﻿using System.Collections;
using FishNet.Object;

namespace TennisProj.Scripts.Gameplay.Abilities
{
    public class BaseAbility : NetworkBehaviour
    {
        public IEnumerator Run()
        {
            HideView();
            yield return Execute();
            Destroy(gameObject);
        }
        
        private void HideView()
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }

        protected virtual IEnumerator Execute()
        {
            yield break;
        }
    }
}