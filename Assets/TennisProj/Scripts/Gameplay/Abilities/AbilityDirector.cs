﻿using System.Collections;
using System.Collections.Generic;
using FishNet.Managing;
using TennisProj.Scripts.Core;
using UniRx;
using UnityEngine;
using VContainer.Unity;

namespace TennisProj.Scripts.Gameplay.Abilities
{
    public class AbilityDirector : ITickable
    {
        private readonly NetworkManager networkManager;
        private readonly GameManager gameManager;
        private readonly BallsDirector ballsDirector;
        private readonly PrefabsFactory prefabsFactory;
        private readonly CoroutineRunner coroutineRunner;

        private List<BaseAbility> spawnedAbilities;
        
        public AbilityDirector(NetworkManager networkManager, GameManager gameManager,  BallsDirector ballsDirector, PrefabsFactory prefabsFactory, CoroutineRunner coroutineRunner)
        {
            this.networkManager = networkManager;
            this.gameManager = gameManager;
            this.ballsDirector = ballsDirector;
            this.prefabsFactory = prefabsFactory;
            this.coroutineRunner = coroutineRunner;

            spawnedAbilities = new List<BaseAbility>();
            
            gameManager.gameState.Subscribe(OnGameStateChanged);
        }

        private void OnGameStateChanged(GameState gameState)
        {
            if (gameState != GameState.Game || !networkManager.IsServer)
                return;

            gameManager.StartCoroutine(RandomAbilitySpawn());
        }

        IEnumerator RandomAbilitySpawn()
        {
            while (gameManager.gameState.Value != GameState.End)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(10, 20));

                if (spawnedAbilities.Count == 0)
                {
                    var randomPos =
                        UnityEngine.Random.Range(0, 10) % 2 == 0
                            ? UnityEngine.Random.Range(-1.3f, -0.8f)
                            : UnityEngine.Random.Range(0.8f, 1.3f);
                    
                    var spawnPos = new Vector3(randomPos, 0, 0);
                    spawnedAbilities.Add(prefabsFactory.CreateRandomAbility(spawnPos));
                }
            }
        }

        public void Tick()
        {
            if (!networkManager.IsServer || gameManager.gameState.Value != GameState.Game)
                return;
            
            CheckBallsOverlapAbilities();
        }

        public void CheckBallsOverlapAbilities()
        {
            var spawnedBalls = ballsDirector.GetSpawnedBalls();
            if(spawnedBalls.Count == 0 || spawnedAbilities.Count == 0)
                return;
            
            for (int i = 0; i < spawnedBalls.Count; i++)
            {
                for (int j = 0; j < spawnedAbilities.Count; j++)
                {
                    if ((spawnedBalls[i].transform.position - spawnedAbilities[j].transform.position).magnitude < 0.3f)
                    {
                        coroutineRunner.StartCoroutine(spawnedAbilities[j].Run());
                        spawnedAbilities.RemoveAt(j);
                        return;
                    }
                }
            }
        }
    }
}