using System.Collections;
using TennisProj.Scripts.Core;
using UnityEngine;
using VContainer;

namespace TennisProj.Scripts.Gameplay.Abilities
{
    public class SpeedUpBallAbility : BaseAbility
    {      
        [Inject] private PlayersDirector playersDirector;
        [Inject] private BallsDirector ballsDirector;

        [SerializeField] private float speedBoostMultiplier = 2;
        [SerializeField] private float speedBoostDuration = 10;
        
        protected override IEnumerator Execute()
        {
            ballsDirector.SetSpeedMultiplier(speedBoostMultiplier);
            yield return new WaitForSeconds(speedBoostDuration);
            ballsDirector.SetSpeedMultiplier(1.0f);
            
            Destroy(this.gameObject);
        }
    }
}