using System.Collections;
using TennisProj.Scripts.Core;
using UnityEngine;
using VContainer;

namespace TennisProj.Scripts.Gameplay.Abilities
{
    public class DoubleSizePlayerAbility : BaseAbility
    {
        [Inject] private PlayersDirector playersDirector;
        [Inject] private BallsDirector ballsDirector;

        [SerializeField] private float duration = 1.0f; 

        protected override IEnumerator Execute()
        {
            var abilityOwner = ballsDirector.LastPlayerBallHit;
            var player = playersDirector.GetPlayerById(abilityOwner.OwnerId);
            var oldLocalScale = player.transform.localScale;
            
            var newLocalScale = new Vector3(
                oldLocalScale.x * 2, 
                oldLocalScale.y,
                oldLocalScale.z);
            
            var controller = player.GetComponent<PlayerController>();
            var targetConnection = controller.ServerManager.Clients[controller.OwnerId];
            
            controller.SyncScaleX(targetConnection, newLocalScale.x);
            yield return new WaitForSeconds(duration);
            controller.SyncScaleX(targetConnection, oldLocalScale.x);
            
            Destroy(this.gameObject);
        }
    }
}