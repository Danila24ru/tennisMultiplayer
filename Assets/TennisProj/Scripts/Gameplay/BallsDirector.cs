﻿using System.Collections.Generic;
using System.Linq;
using FishNet.Object;
using TennisProj.ScriptableObjects;
using TennisProj.Scripts.Core;
using UniRx;
using UnityEngine;
using VContainer.Unity;

namespace TennisProj.Scripts.Gameplay
{
    
    public class BallsDirector : ITickable
    {
        private readonly PrefabsFactory prefabsFactory;
        private readonly GameSettings gameSettings;
        private readonly PlayersDirector playerDirector;

        public NetworkObject LastPlayerBallHit;
        
        private List<Ball> spawnedBalls;

        private float speedMultiplier = 1;

        public BallsDirector(PrefabsFactory prefabsFactory, GameSettings gameSettings, PlayersDirector playersDirector)
        {
            this.prefabsFactory = prefabsFactory;
            this.gameSettings = gameSettings;
            this.playerDirector = playersDirector;

            spawnedBalls = new List<Ball>();
        }

        public Ball AddBall()
        {
            var ball = prefabsFactory.CreateBall(Vector3.zero);
            InitBall(ball);
            spawnedBalls.Add(ball);
            return ball;
        }

        public List<Ball> GetSpawnedBalls() => spawnedBalls;

        private void InitBall(Ball ball)
        {
            ball.currentVelocity = gameSettings.ballSettings.initialSpeed;

            var initialAngle = Random.Range(30, 150);
            var initialSide = Random.Range(0, 10) % 2 == 0 ? 1 : -1;

            ball.currentMoveDirection = new Vector3(initialAngle * initialSide, 0, initialSide * 50).normalized;
        }
        
        public void Tick()
        {
            for (int i = 0; i < spawnedBalls.Count; i++)
            {
                UpdateBall(spawnedBalls[i]);
            }
        }
        
        private void UpdateBall(Ball ball)
        {
            if (!ball.isActive)
                return;

            MoveBall(ball, speedMultiplier);
            
            if (ball.transform.position.x >= 1.9)
                ball.currentMoveDirection = Vector3.Reflect(ball.currentMoveDirection, Vector3.left);

            if (ball.transform.position.x <= -1.9)
                ball.currentMoveDirection = Vector3.Reflect(ball.currentMoveDirection, Vector3.right);

            var firstPlayer = playerDirector.GetPlayerById(0);
            if (ball.transform.position.z >= 2.15 && firstPlayer != null &&
                Mathf.Abs(ball.transform.position.x - firstPlayer.transform.position.x) < firstPlayer.transform.localScale.x / 2f)
            {
                LastPlayerBallHit = firstPlayer;
                ball.currentMoveDirection = Vector3.Reflect(ball.currentMoveDirection, Vector3.back);
                MoveBall(ball, 3);
            }

            var secondPlayer = playerDirector.GetPlayerById(1);
            if (ball.transform.position.z <= -2.15 && secondPlayer != null &&
                Mathf.Abs(ball.transform.position.x - secondPlayer.transform.position.x) < secondPlayer.transform.localScale.x / 2f)
            {
                LastPlayerBallHit = secondPlayer;
                ball.currentMoveDirection = Vector3.Reflect(ball.currentMoveDirection, Vector3.forward);
                MoveBall(ball, 3);
            }
        
            if (ball.transform.position.z > 2.4)
            {
                MessageBroker.Default.Publish(new GoalSignal { isTop = true });
                DestroyBall(ball);
            }
        
            if(ball.transform.position.z < -2.4)
            {
                MessageBroker.Default.Publish(new GoalSignal { isTop = false });
                DestroyBall(ball);
            }
        }

        private void MoveBall(Ball ball, float multiplier = 1)
        {
            if (ball.currentVelocity < gameSettings.ballSettings.maxSpeed)
                ball.currentVelocity = ball.currentVelocity + gameSettings.ballSettings.acceleration * Time.deltaTime;
        
            ball.transform.position += ball.currentMoveDirection * ball.currentVelocity * Time.deltaTime * multiplier;
        }

        public void SetSpeedMultiplier(float speedMultiplier) => this.speedMultiplier = speedMultiplier;

        public void ClearAllBalls()
        {
            foreach (var ball in spawnedBalls.ToList())
            {
                DestroyBall(ball);
            }
        }

        void DestroyBall(Ball ball)
        {
            spawnedBalls.Remove(ball);
            Object.Destroy(ball.gameObject);
        }
    }
}