using FishNet.Object;
using UnityEngine;

namespace TennisProj.Scripts.Gameplay
{
    public class Ball : NetworkBehaviour
    {
        public bool isActive;
        public float currentVelocity;
        public Vector3 currentMoveDirection;
    }
}
