using FishNet.Connection;
using FishNet.Object;
using TennisProj.ScriptableObjects;
using TennisProj.Scripts.Core;
using UnityEngine;
using VContainer;

namespace TennisProj.Scripts.Gameplay
{
    public class PlayerController : NetworkBehaviour
    {
        private GameSettings gameSettings;

        public Vector3 playerScreenPos;
        public Vector3 mousePos;

        private float currentVelocity;

        public override void OnStartClient()
        {
            base.OnStartClient();
        
            gameSettings = FindObjectOfType<Bootstrap>().Container.Resolve<GameSettings>();
        
            if (!IsServer)
            {
                Camera.main.transform.position = new Vector3(0, 4, -2.8f);
                Camera.main.transform.eulerAngles = new Vector3(60, 0, 0);
            } 
        }

        private void Update()
        {
            if (!IsOwner)
                return;
        
            playerScreenPos = Camera.main.WorldToScreenPoint(transform.position);
            mousePos = Input.mousePosition;

            var dif = mousePos.x - playerScreenPos.x;

            if (Mathf.Abs(dif) < 3f)
            {
                currentVelocity = 0;
                return;
            }

            var direction = 
                dif >= 0.1f ? (IsServer ? -1.0f : 1.0f) :
                dif <= 0.1f ? (IsServer ? 1.0f : -1.0f) : 0.0f;

            if (currentVelocity < gameSettings.playerSettings.maxSpeed)
                currentVelocity = currentVelocity + gameSettings.playerSettings.acceleration * Time.deltaTime;
        
            var newPos = transform.position + new Vector3(direction * currentVelocity * Time.deltaTime, 0, 0);
        
            if (newPos.x + direction * (transform.localScale.x / 2) is > 1.9f or < -1.9f)
                return;

            transform.position = newPos;
        }

        [TargetRpc]
        public void SyncScaleX(NetworkConnection conn, float scaleX)
        {
            var localScale = transform.localScale;
            localScale.x = scaleX;
            transform.localScale = localScale;
        }
    }
}
