using FishNet.Object;
using FishNet.Object.Synchronizing;

namespace TennisProj.Scripts.Gameplay
{
    public class PlayerScore : NetworkBehaviour
    {
        [SyncVar]
        public int Score;

        public void IncrementScore() => Score += 1;
    }
}
